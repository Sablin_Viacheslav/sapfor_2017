#pragma once
#include <vector>

#include "dvm.h"
#include "../Utils/errors.h"

void convertFromAssignToLoop(SgFile *file, const std::vector<ParallelRegion*>& regions, std::vector<Messages> &messagesForFile, const std::map<DIST::Array*, std::set<DIST::Array*>>& arrayLinksByFuncCalls);
void restoreAssignsFromLoop(SgFile *file, const std::map<DIST::Array*, std::set<DIST::Array*>>& arrayLinksByFuncCalls);
void restoreConvertedLoopForParallelLoops(SgFile *file, const std::map<DIST::Array*, std::set<DIST::Array*>>& arrayLinksByFuncCalls, bool reversed = false);
bool notDeletedVectorAssign(SgStatement* st);
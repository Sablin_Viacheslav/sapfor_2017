#pragma once

bool checkOutCalls(const std::set<std::string>& outCalls);
std::map<SgStatement*, std::set<std::string>> fillFromIntent(SgStatement* header);
void intentInsert(const std::vector<FuncInfo*>& allFuncInfo);
void intentInsertToInterfaces(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);
void setPureStatus(const std::set<FuncInfo*>& funcInfo);
void setPureStatus(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);

void commonTransfer(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo, const std::map<std::string, CommonBlock>& commonBlocks);
void saveTransfer(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);
void moduleTransfer(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);
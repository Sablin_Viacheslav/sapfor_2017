#pragma once

#include "dvm.h"
#include "../GraphLoop/graph_loops.h"
#include "../SageAnalysisTool/depGraph.h"
#include "../SageAnalysisTool/OmegaForSage/include/lang-interf.h"
#include "../DirectiveProcessing/directive_parser.h"
#include <vector>

int combineLoops(SgFile *file, std::vector<LoopGraph*> &loopGraphs, std::vector<Messages> &messages, 
	             const std::pair<std::string, int>& onPlace, const std::map<LoopGraph*, depGraph*>& depInfoForLoopGraph);
